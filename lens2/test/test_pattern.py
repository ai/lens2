import os
import unittest
from lens2 import pattern


def writefile(path, contents):
    fd = open(path, 'w')
    fd.write(contents)
    fd.close()


class PatternTest(unittest.TestCase):

    def setUp(self):
        self.ptfile = 'patterns-%d.conf' % os.getpid()
        writefile(self.ptfile, '''
# test pattern file
failed login for @USER@ from @HOST@
nice (regex $syntax [error
''')
        self.pe = pattern.PatternExtractor(self.ptfile)

    def tearDown(self):
        os.remove(self.ptfile)

    def test_setup_sanity_check(self):
        # If the syntax error was not ignored, length will be != 1.
        self.assertEquals(1, len(self.pe.patterns))

    def test_simple_pattern(self):
        log = {'msg': 'failed login for root from 127.0.0.1'}
        self.pe(log)

        self.assert_('msg' in log,
                     'Meta propagation error, original data is gone')
        self.assertEquals('root', log['attr_user'])
        self.assertEquals('127.0.0.1', log['attr_host'])


if __name__ == '__main__':
    unittest.main()

