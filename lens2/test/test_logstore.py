import os
import unittest
import logging
import datetime

import mox
import pyes
import pyes.managers
from pyes.exceptions import IndexMissingException
from lens2 import logstore, syslog_parser, utils


class FakeResultSet(object):

    def __init__(self, count=5):
        self._count = count
        self.total = count
        self.took = 0.1
        self._result = []
        for x in range(count):
            self._result.append({'_source': 'fake %d' % x})

    def __iter__(self):
        return iter(self._result)


class LogStoreTest(mox.MoxTestBase):

    def _index_name(self, when=None):
        if when is None:
            when = datetime.datetime.utcnow()
        return when.strftime('logs-%Y.%m.%d')

    def _gen_indices(self, days):
        """Generate index names for the last n days."""
        now = datetime.datetime.utcnow()
        for i in xrange(days):
            yield self._index_name(now - datetime.timedelta(days=i))

    def test_expire_indices(self):
        """Test expire 5d logs having 10d logs."""
        days_available = 10
        days_delete = 5

        indices_available = list(self._gen_indices(days_available))
        indices_delete = list(self._gen_indices(days_delete))
        fake_indices = self.mox.CreateMock(pyes.managers.Indices)
        fake_indices.aliases().AndReturn(dict([(x, True) for x in indices_available]))
        for x in set(indices_available) - set(indices_delete):
            fake_indices.delete_index(x).InAnyOrder()

        self.mox.StubOutClassWithMocks(pyes, 'ES')
        es = pyes.ES('server', timeout=5, bulk_size=logstore.LogStore.BULK_SIZE)
        es.indices = fake_indices
        self.mox.ReplayAll()

        parser = syslog_parser.get_parser('dumb')
        ls = logstore.LogStore('server', timeout=5)
        since = utils.parse_timespec('%dd' % days_delete)
        ls.expire(since)
        self.mox.VerifyAll()

    def test_search_indices(self):
        """Test searching 3d ago having 10d logs."""
        days_available = 10
        days_lookback = 3

        indices_available = list(self._gen_indices(days_available))
        indices_lookback = list(self._gen_indices(days_lookback+1))
        fake_indices = self.mox.CreateMock(pyes.managers.Indices)
        fake_indices.aliases().AndReturn(dict([(x, True) for x in indices_available]))
        self.mox.StubOutClassWithMocks(pyes, 'ES')
        es = pyes.ES('server', timeout=5, bulk_size=logstore.LogStore.BULK_SIZE)
        self.mox.StubOutWithMock(es, 'search')
        es.indices = fake_indices
        es.search(mox.IgnoreArg(),
                indices=mox.SameElementsAs(indices_lookback),
                model=mox.IgnoreArg(),
                search_type=mox.IgnoreArg()
            ).AndReturn(FakeResultSet())
        self.mox.ReplayAll()

        parser = syslog_parser.get_parser('dumb')
        ls = logstore.LogStore('server', timeout=5)
        time_range = utils.parse_time_range('%dd' % days_lookback)
        ls.search('query', time_range, 0, 400)
        self.mox.VerifyAll()

    def test_create_index(self):
        """Test creation of a non-existing index."""
        fake_indices = self.mox.CreateMock(pyes.managers.Indices)
        fake_indices.open_index('logs-2014.02.23').AndRaise(IndexMissingException('foo'))
        fake_indices.create_index_if_missing('logs-2014.02.23')
        fake_indices.put_mapping('log', mox.IsA(dict), 'logs-2014.02.23')

        self.mox.StubOutClassWithMocks(pyes, 'ES')
        es = pyes.ES('server', timeout=5, bulk_size=logstore.LogStore.BULK_SIZE)
        self.mox.StubOutWithMock(es, 'force_bulk')
        self.mox.StubOutWithMock(es, 'index')
        self.mox.StubOutWithMock(es, '_send_request')
        es.indices = fake_indices
        es._send_request('PUT', '/_template/tpl1', mox.IsA(dict))
        es.index(mox.IsA(dict), 'logs-2014.02.23', 'log', bulk=True)
        es.force_bulk()
        self.mox.ReplayAll()

        parser = syslog_parser.get_parser('dumb')
        ls = logstore.LogStore('server', timeout=5)
        ls.insert("Feb 23 18:49:50 void prog[666]: message", parser)
        self.mox.VerifyAll()

    def test_open_index(self):
        """Test open of an existing index."""
        fake_indices = self.mox.CreateMock(pyes.managers.Indices)
        fake_indices.open_index('logs-2014.02.23')

        self.mox.StubOutClassWithMocks(pyes, 'ES')
        es = pyes.ES('server', timeout=5, bulk_size=logstore.LogStore.BULK_SIZE)
        self.mox.StubOutWithMock(es, 'force_bulk')
        self.mox.StubOutWithMock(es, 'index')
        es.indices = fake_indices
        es.index(mox.IsA(dict), 'logs-2014.02.23', 'log', bulk=True)
        es.force_bulk()
        self.mox.ReplayAll()

        parser = syslog_parser.get_parser('dumb')
        ls = logstore.LogStore('server', timeout=5)
        ls.insert("Feb 23 18:49:50 void prog[6666]: message", parser)
        self.mox.VerifyAll()


if __name__ == '__main__':
    #logging.basicConfig(level=logging.DEBUG)
    unittest.main()
