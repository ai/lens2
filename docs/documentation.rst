
.. contents::



Overview
--------

Lens is a tool for log collection and analysis. It is meant primarily
for logs in Syslog format, leveraging an existing syslog collection
infrastructure, but it is possible to extend it by writing parsers for
new log formats (as long as they are line-based).

It has a few useful features:

* **Full text indexing and search**, the data store is Elasticsearch_
* **Attribute extraction** using a simple regex-based configuration
  language, see `Pattern Extraction`_ 
* **Web UI** for live analysis

To use Lens, an existing syslog server is required (it can be a
dedicated instance, for remote log collection, for example), which
needs to be able to write to a local FIFO. Pretty much any syslog
server should satisfy this requirement, but it will be necessary to
configure it appropriately. See `Integration With Syslog`_ for details
on how to do so.

The other requirement is an Elasticsearch_ installation. An
Elasticsearch cluster is quite easy to setup and configure (see the
`ES installation docs`_), but for testing purposes all you need is to
download the package and run::

    $ bin/elasticsearch

This will start an ES instance on ``localhost:9200``.



Installation
------------

To install Lens the only requirement is a Python interpreter (at least
version 2.5), and `setuptools`_. For Debian-based systems, this
amounts to::

    $ sudo apt-get install python python-setuptools



Install From Source
~~~~~~~~~~~~~~~~~~~

1. Clone the Lens repository::

     $ git clone https://git.autistici.org/ai/lens2.git

2. Run the ``setup.py`` installation script::

     $ cd lens2 && sudo python setup.py install



Install From Debian Package
~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Add the repository to your APT config by placing the following
   contents in a file such as ``/etc/apt/sources.list.d/lens2.list``::

     deb http://git.autistici.org/p/lens2/debian unstable main

2. Install the repository key::

     $ wget -O- http://git.autistici.org/p/lens2/debian/repo.key | \
         sudo apt-key add -

3. Install the ``lens2`` package::

     $ sudo apt-get install lens2

The Debian package will install init scripts to control the indexing
daemon in ``/etc/init.d/lens2``.


Deployment
----------

Lens consists of two components:

- the indexing daemon, which reads logs from the syslog server and
  stores them into the Elasticsearch index

- the web application, which serves user queries and provides a JSON
  API to the index.

These two components are completely independent from each other, so
they can be deployed separately (even on different machines). For
testing purposes, and for very small organizations, you can run the
full stack on a single server (the syslog collector, the Elasticsearch
instance, the lens daemon and a web server), but it's trivial to scale
the system up if necessary (see `Scaling`_).



Integration With Syslog
~~~~~~~~~~~~~~~~~~~~~~~

The lens daemon is meant to be connected to a syslog server, usually
a 'collector' for your remote logs, via a FIFO.

Lens supports a number of syslog-like formats for its input:

``iso``
  A log format with ISO-formatted timestamps and that contains
  priority and facility information. For instance, this is the
  syslog-ng template::

    template("${S_ISODATE} ${HOST} ${FACILITY}.${PRIORITY} ${MSGHDR}${MSG}\n")

  and this one is for rsyslog::

    $template localFormat,"%timereported:::date-rfc3339% %HOSTNAME% %syslogfacility-text%.%syslogseverity-text% %syslogtag%%msg::space%\n"

  This is also the default format.

``standard``
  Log format with a traditional syslog timestamp (i.e. "Feb 10
  11:12:13") but with facility and priority (separated by a dot)
  following the host name.

``dumb``
  The default syslog format. Its usage is discouraged, since it lacks
  facility and priority information. It is provided to allow importing
  traditional syslog log files for testing purposes.

An example configuration file for syslog-ng is provided in
``docs/syslog-ng.conf.sample`` in the source tree.

A note on FIFO delivery: most modern syslog servers provide features
to buffer logs in memory, or on disk, for destinations that are
temporarily unavailable or blocked. This should be enabled for the
Lens FIFO destination, to support seamless restarts of the indexer
daemon. Furthermore, the indexer daemon will block reading from the
FIFO whenever the ES backend is unavailable.



Integration With Elasticsearch
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Some maintenance tasks on the Elasticsearch cluster will be required. For
instance, Lens periodically removes old logs from the index; unless the
Elasticsearch index is optimized once in a while, it will grow unbounded. Logs
are sharded into per-day indices which will need to be periodically expired
using `lens2 expire` command.




Web Application Deployment
~~~~~~~~~~~~~~~~~~~~~~~~~~

The Lens web UI is a pretty simple WSGI application (written using
`Flask`_) implementing a JSON API to query the underlying
Elasticsearch index. The web UI is mostly implemented in the browser
using Javascript.

The configuration is stored in a Python file containing variable
assignments. Besides various possible Flask configuration options, the
only variable that Lens needs is ``ES_SERVER``, which should contain
either a string with the ``HOST:PORT`` address of the ES server, or a
list of them (the Python ES client library will perform automatic
server failover/load balancing in this case).

The deployment strategy will vary depending on your web server
environment: check out the `Flask deployment documentation`_ for the
specific details. We do provide some example handlers for common
configurations though:

- **FastCGI** in ``docs/lens.fcgi`` (requires `flup`_)
- **mod_wsgi** in ``docs/lens.wsgi``

The Lens web UI is completely stateless and uses a minimal amount of
resources (the queries are simply forwarded to Elasticsearch), so it
can easily scale horizontally, and it trivially supports HA
configurations.



Pattern Extraction
------------------

You can define patterns with placeholders that, when matched against a
record, will cause it to be tagged with extra attributes.  Those
attributes are indexed and can be included in a search query.

Patterns are regular expressions: placeholders are specified by
wrapping the attribute name (uppercase) within '@' symbols, and they
will be replaced by a '(.*)' match in the final regular expression.

For example, to match failed SSH logins, and extract the *user* and
*ip* attributes, you could use the following pattern::

    Failed password for @USER@ from @IP@

It is also possible to specify the regular expression that will be
substituted for the placeholder variable by using the
``@NAME:REGEXP@`` syntax, i.e.::

    Failed password for @USER@ from @IP:[0-9.]+@

A *pattern file* simply contains a list of such patterns, one by row.
Empty lines are ignored, and lines starting with ``#`` are considered
comments. Pattern files are searched for by default in
``/etc/lens/patterns.d``, using *run-parts* semantics (file names
containing dots are ignored).



Scaling
-------

The tool is designed to scale to a reasonably large number of systems:
since it acts as a stateless connector between systems that can be
scaled independently (the syslog collection infrastructure on one
side, and the Elasticsearch cluster on the other), it is trivial to
partition it following the growth of those systems.

To give a general idea of performance, we run a testing instance on a
single machine, with a log database consisting of ~40M records, and it
can reliably insert upwards of 2500 logs/sec.



Links
-----

A project that is very similar to Lens, but bigger and better, is
Logstash_.




.. _Elasticsearch: http://www.elasticsearch.org/
.. _ES installation docs: http://www.elasticsearch.org/guide/reference/setup/installation.html
.. _setuptools: http://pypi.python.org/pypi/setuptools
.. _Flask: http://flask.pocoo.org/
.. _Flask deployment documentation: http://flask.pocoo.org/docs/deploying/
.. _flup: http://pypi.python.org/pypi/flup
.. _Logstash: http://www.logstash.net/
