#!/usr/bin/python

from setuptools import setup, find_packages

setup(
  name="lens2",
  version="0.3",
  description="Elasticsearch-based log indexer for syslog",
  author="ale",
  author_email="ale@incal.net",
  url="https://git.autistici.org/ai/lens2",
  setup_requires=[],
  zip_safe=False,
  packages=find_packages(),
  package_data={"lens2":["templates/**", "static/**"]},
  include_package_data=True,
  entry_points={
    "console_scripts": [
      "lens2 = lens2.main:main",
      "lens2syslog = lens2.lens2syslog:main",
    ],
  },
  )

