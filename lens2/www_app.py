from flask import Flask
from lens2 import logstore
from lens2 import www_api
from lens2 import www_ui


def make_app(config={}):
    app = Flask(__name__)
    app.config.update(config)
    app.config.from_envvar('APP_SETTINGS', silent=True)

    app.register_blueprint(www_api.api_app)
    app.register_blueprint(www_ui.ui_app)

    server_list = config.get('ES_SERVER', 'localhost:9200')
    app.logstore = logstore.LogStore(server_list)

    app.jinja_env.globals['version'] = '0.3'
    return app
