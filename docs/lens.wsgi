# Sample mod_wsgi driver for lens2.
#
# Example Apache2 configuration snippet:
#
#     Alias /static /var/www/static
#     SetEnv APP_SETTINGS /etc/lens/app.conf
#     WSGIDaemonProcess lens2 threads=5
#     WSGIScriptAlias / /var/www/lens.wsgi

from lens2 import www_app
application = www_app.make_app()
