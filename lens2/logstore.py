import datetime
import logging
import os
import time
from lens2 import utils

import elasticsearch
import elasticsearch.helpers
from elasticsearch_dsl import Search


log = logging.getLogger(__name__)


def to_timestamp(t):
    if isinstance(t, datetime.datetime):
        t = time.mktime(t.utctimetuple())
    return int(t * 1000000)


def from_timestamp(t):
    return datetime.datetime.utcfromtimestamp(t / 1000000.0)


class NoIndicesFound(Exception):
    pass


class LogStore(object):
    """Interface to the log database.

    The LogStore provides facilities to add and search/scan logs in
    the underlying database (the current implementation uses
    ElasticSearch for this).
    """

    INDEX_PREFIX = 'logs'
    BULK_SIZE = 400

    def __init__(self, server_list, timeout=60):
        self.conn = elasticsearch.Elasticsearch(server_list, timeout=timeout)
        self._open_indices = {}

    def _open_index(self, index_name):
        if index_name in self._open_indices:
            return

        try:
            log.info('opening index %r', index_name)
            self.conn.indices.open(index_name)
        except elasticsearch.NotFoundError:
            self._init_index(index_name)

        self._open_indices[index_name] = True
        return

    @classmethod
    def _index_from_timestamp(cls, t):
        t = from_timestamp(t)
        return cls.INDEX_PREFIX + '-' + t.strftime('%Y.%m.%d')

    @classmethod
    def _index_from_time_range(cls, time_range):
        start = to_timestamp(time_range[0] or 0)
        end = to_timestamp(time_range[1] or time.time())
        res = []
        while end >= start:
            res.append(cls._index_from_timestamp(end))
            end -= 86400 * 1000000
        return res

    def _datetime_from_index(self, index):
        try:
            d = datetime.datetime.strptime(index, self.INDEX_PREFIX + '-' + '%Y.%m.%d')
            return d
        except ValueError:
            return None

    def clear(self):
        """"Remove all indices."""
        for index in self._valid_indices():
            self.conn.indices.delete(index)

    def get_status(self):
        """Return the index status."""
        indices = self.open_indices.keys()
        # XXX fix, does it work?
        status = self.conn.indices.status(indices=indices)
        return [status['indices'][x] for x in indices]

    def _init_index(self, index_name):
        """Create the index and set up the schema."""
        log.info('creating index %r', index_name)
        self.conn.indices.create(index=index_name, ignore=404)
        # XXX Other than where documented, existing type and field mappings cannot be updated.
        # https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-put-mapping.html#updating-field-mappings
        default_mappings = {
            'log': {
                '_source': {
                    'compress': True,
                },
                'date_detection': False,
                'dynamic_templates': [
                    {
                        'template1': {
                            'match': 'attr_*',
                            'match_mapping_type': 'string',
                            'mapping': {
                                'type': 'string',
                                'index': 'analyzed',
                                'store': 'no',
                                'include_in_all': 'no',
                                },
                            },
                        }],
                },
            }
        template = {
                'template': index_name,
                'mappings': default_mappings,
            }
        self.conn.indices.put_template(name='tpl1', body=template, create=True)
        log_mapping = {
            'timestamp': {
                'index': 'not_analyzed',
                'type': 'long',
                'store': 'no',
                'include_in_all': 'no',
                },
            'host': {
                'index': 'not_analyzed',
                'store': 'no',
                'type': 'string',
                'include_in_all': 'no',
                },
            'facility': {
                'index': 'not_analyzed',
                'store': 'no',
                'type': 'string',
                'include_in_all': 'no',
                },
            'severity': {
                'index': 'not_analyzed',
                'store': 'no',
                'type': 'string',
                'include_in_all': 'no',
                },
            'program': {
                'index': 'not_analyzed',
                'store': 'no',
                'type': 'string',
                },
            'msg': {
                'index': 'analyzed',
                'store': 'yes',
                'type': 'string',
                'term_vector': 'with_positions_offsets',
                },
            }
        self.conn.indices.put_mapping(doc_type='log', body={'properties': log_mapping}, index=index_name)

    def insert(self, line_or_lines, parser, filters=None):
        """Insert one or more logs.

        Args:
          line_or_lines: a string, or an iterable generating log lines
          parser: parser function, must return a dictionary when called
              with an input line
          filters: a list of filter functions, used to manipulate the
              parsed dictionaries

        Returns:
          A (total_records, num_errors) tuple of integers.
        """
        if isinstance(line_or_lines, basestring):
            line_or_lines = [line_or_lines]

        start = time.time()
        stats = {'n_records': 0, 'n_db_errors': 0, 'n_parse_errors': 0}

        def to_doc(stream):
            for line in stream:
                try:
                    doc = parser(line)
                    if filters:
                        for f in filters:
                            doc = f(doc)
                    doc['timestamp'] = to_timestamp(doc['timestamp'])
                    yield doc
                except Exception, e:
                    log.exception('parse error: %r', e)
                    stats['n_parse_errors'] += 1

        def to_bulk(stream):
            for doc in to_doc(stream):
                doc['_index'] = self._index_from_timestamp(doc['timestamp'])
                doc['_type'] = 'log'
                # XXX pass in also _id ?
                yield doc

        success, errors = elasticsearch.helpers.bulk(self.conn,
                                                     to_bulk(line_or_lines),
                                                     chunk_size=self.BULK_SIZE,
                                                     raise_on_error=False,
                                                     raise_on_exception=False)
        stats['n_db_errors'] += len(errors)
        stats['n_records'] += success

        elapsed = time.time() - start
        log.info('inserted %d docs (errors: db=%d, parse=%d) in %g seconds, '
                 '%.5g docs/s',
                 stats['n_records'], stats['n_db_errors'],
                 stats['n_parse_errors'], elapsed,
                 stats['n_records'] / elapsed)
        return (stats['n_records'],
                stats['n_db_errors'] + stats['n_parse_errors'])

    def expire(self, timelimit):
        """Remove all logs older than a certain time.

        Args:
          timelimit: a timestamp indicating the cutoff absolute time
        """
        def _expire_indices(timelimit):
            """Delete existing indices up until timelimit."""
            for index in self._valid_indices():
                index_date = self._datetime_from_index(index)
                if index_date is None or index_date >= timelimit:
                    continue
                log.info('deleting %r', index)
                self.conn.indices.delete(index)

        timediff = datetime.datetime.utcnow() - timelimit
        # XXX fix for es 2.0
        if timediff.days == 0:
            trange = pyes.utils.ESRange(
                'timestamp',
                from_value=0,
                to_value=to_timestamp(timelimit))
            f = pyes.filters.NumericRangeFilter(trange)
            q = pyes.query.FilterQuery(pyes.query.MatchAllQuery(), f)
            index = self._index_from_timestamp(timelimit)
            self.conn.delete_by_query(index, ['log'], q)
            self.conn.optimize(index)
        else:
            _expire_indices(timelimit)

    def _make_search(self, query_str, time_range):
        indices = list(self._valid_indices())
        if time_range:
            wanted_indices = self._index_from_time_range(time_range)
            indices = set(wanted_indices) & set(indices)

        if not indices:
            e = NoIndicesFound('no indices for search %r range=%s' %
                    (query_str, time_range))
            log.exception(e)
            raise e

        s = Search(using=self.conn, index=','.join(indices))
        s = s.params(search_type='query_then_fetch')
        s = s.query("query_string", query=query_str)
        if time_range:
            s = s.filter('range',
                    **{'timestamp': {
                        'gte': to_timestamp(time_range[0] or 0),
                        'lte': to_timestamp(time_range[1] or time.time()),
                        }})
        return s.sort('-timestamp')

    def _valid_indices(self):
        # XXX check return value
        for x in self.conn.indices.get_aliases():
            if x.startswith(self.INDEX_PREFIX + '-'):
                yield x


    def search(self, query_str, time_range, size=100, start=0, facets=None):
        log.debug('search: "%s", range=%s, start=%d, facets=%s',
                  query_str, time_range, start, facets)
        search = self._make_search(query_str, time_range)[start:size]
        if not search:
            return [], 0, {}, 0

        # XXX reenable
        facets = None
        if facets:
            for f, fsize in facets:
                if f == 'timestamp':
                    search.facet.facets.append(
                        pyes.facets.HistogramFacet(
                            'timestamp',
                            field='timestamp',
                            interval=86400 * 1000000))
                else:
                    search.facet.add_term_facet(f, size=fsize)

        result = search.execute()
        docs = [x.to_dict() for x in result]
        result_facets = result.facets if facets else {}
        return docs, result.hits.total, result_facets, result.took

    def scan(self, query_str, time_range, size=100):
        log.info('scan: %r', query_str)
        search = self._make_search(query_str, time_range)[:size]
        if not search:
            yield None
        else:
            for doc in search.scan():
                yield doc
