import logging
import optparse
import sys
from lens2 import daemonize
from lens2 import ioutil
from lens2 import logstore
from lens2 import pattern
from lens2 import syslog_parser
from lens2 import utils
from lens2 import www_app

log = logging.getLogger(__name__)

DEFAULT_ES_URL = 'localhost:9200'
SYSLOG_PARSERS = syslog_parser.list_parsers()


def run_http_server(lens, opts, args):
    app = www_app.make_app()
    app.run(port=opts.http_port)


def create_syslog_parser(opts):
    try:
        return syslog_parser.get_parser(opts.log_format)
    except KeyError:
        raise Exception('Unsupported log format "%s"' % opts.log_format)


def run_daemon(lens, opts, args):
    parser = create_syslog_parser(opts)
    filters = None
    if opts.pattern_file:
        filters = [pattern.PatternExtractor(opts.pattern_file)]
    while True:
        try:
            inputf = ioutil.read_fifo(opts.fifo)
            lens.insert(inputf, parser, filters)
        except Exception, e:
            log.error('FIFO reader died with exception: %s', str(e))


def run_inject(lens, opts, args):
    parser = create_syslog_parser(opts)
    filters = None
    if opts.pattern_file:
        filters = [pattern.PatternExtractor(opts.pattern_file)]
    for filename in args:
        logging.info('injecting %s', filename)
        lens.insert(ioutil.read_file(filename), parser, filters)


def do_search(lens, opts, args):
    query = ' '.join(args)
    time_range = None
    if opts.time_range:
        time_range = utils.parse_time_range(opts.time_range)
    scroll_id = None
    count = 0
    page_size = 400
    while True:
        results, total, facet_data, elapsed = lens.search(
            query, time_range, start=count, size=page_size)
        if not results:
            break
        log.debug('search: %d results (%d/%d), %g secs', len(results),
                  count, total, elapsed)
        for doc in results:
            sys.stdout.write(utils.format_log(doc).encode('utf-8', 'replace'))
        count += len(results)
        if count >= total:
            break
    sys.stdout.flush()


def do_scan(lens, opts, args):
    query = ' '.join(args)
    time_range = None
    if opts.time_range:
        time_range = utils.parse_time_range(opts.time_range)
    for doc in lens.scan(query, time_range):
        sys.stdout.write(utils.format_log(doc).encode('utf-8', 'replace'))
    sys.stdout.flush()


def do_expire(lens, opts, args):
    since = utils.parse_timespec(args[0])
    if not since:
        return
    lens.expire(since)


def main():
    parser = optparse.OptionParser(usage='''lens2 <COMMAND> [<ARGS>...]

Known commands:
    daemon           Start a daemon that read logs from a FIFO
    http-server      Start a HTTP server
    inject FILES     Insert files into the index
    search QUERY     Search logs
    expire TIMESPEC  Remove old logs from the index
''')
    parser.add_option('--fifo', dest='fifo', metavar='PATH',
                      help='Read input from this FIFO')
    parser.add_option('--patterns', dest='pattern_file', metavar='FILE',
                      help='Read patterns to extract from FILE')
    parser.add_option('--port', dest='http_port', metavar='PORT',
                      default=8081, type='int',
                      help='TCP port for the HTTP server (default 8081)')
    parser.add_option('--range', dest='time_range', metavar='TIMESPEC',
                      default='1d',
                      help='Only search within the specified time range (default %default)')
    parser.add_option('--url', dest='es_url', metavar='URL',
                      default=DEFAULT_ES_URL,
                      help='URL for the ES servers (default %default)')
    parser.add_option('--log-format', dest='log_format',
                      default='standard', metavar='FMT', choices=SYSLOG_PARSERS,
                      help='Log format (%s)' % ', '.join(SYSLOG_PARSERS))
    daemonize.add_options(parser)
    opts, args = parser.parse_args()
    if len(args) < 1:
        parser.error('Please specify a command')

    # XXX turn into a debug option
    logging.basicConfig(level=logging.INFO)
    logging.getLogger("urllib3").setLevel(logging.ERROR)
    logging.getLogger("elasticsearch").setLevel(logging.ERROR)
    logging.getLogger("elasticsearch.trace").setLevel(logging.ERROR)

    lens = logstore.LogStore(opts.es_url, timeout=3600)

    cmd, args = args[0], args[1:]
    if cmd == 'daemon':
        if not opts.fifo:
            parser.error('You must specify --fifo')
        if len(args) != 0:
            parser.error('Too many arguments')
        daemonize.daemonize(opts, run_daemon, lens, opts, args)
    elif cmd == 'http-server':
        if len(args) != 0:
            parser.error('Too many arguments')
        daemonize.daemonize(opts, run_http_server, lens, opts, args)
    elif cmd == 'inject':
        if len(args) < 1:
            parser.error('Too few arguments')
        opts.foreground = True
        daemonize.daemonize(opts, run_inject, lens, opts, args)
    elif cmd == 'search':
        if len(args) < 1:
            parser.error('Too few arguments')
        do_search(lens, opts, args)
    elif cmd == 'scan':
        if len(args) < 1:
            parser.error('Too few arguments')
        do_scan(lens, opts, args)
    elif cmd == 'expire':
        if len(args) != 1:
            parser.error('Syntax: expire <TIMESPEC>')
        do_expire(lens, opts, args)
    elif cmd == 'wipe':
        lens.clear()
        print 'Database wiped.'
    else:
        parser.error('Unknown command')


if __name__ == '__main__':
    main()
