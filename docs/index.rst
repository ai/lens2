
An Elasticsearch-based syslog indexer
=====================================

*lens* is a tool to analyze syslog logs. It is most useful when your
log volume is large enough that simple analysis tools such as 'grep'
and 'awk' just won't cut it anymore.

*lens* is able to extract attributes from logs (according to rules
that you specify) from your log files, that you can then use to filter
search results.

*lens* uses `Elasticsearch`_ as its database backend and search
engine, which provides for most of its useful features.



.. _Elasticsearch: http://www.elasticsearch.org/
