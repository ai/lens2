#!/usr/bin/python
#
# Sample FCGI driver for lens2 (using flup).
#
# Example Apache2 configuration snippet:
#
#      RewriteEngine On
#      FcgidInitialEnv APP_SETTINGS /etc/lens/app.conf
#      RewriteCond /var/www/%{REQUEST_FILENAME} !-f
#      RewriteRule ^(.*)$ /var/www/lens.fcgi$1 [L]
#
# To serve static contents from Apache just make a link from
# /var/www/static to the lens2/static dir of your lens2 installation.

from flup.server.fcgi import WSGIServer
from lens2 import www_app

if __name__ == '__main__':
    application = www_app.make_app()
    WSGIServer(application).run()

