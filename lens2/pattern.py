import glob
import logging
import re

log = logging.getLogger(__name__)


class Pattern(object):

    _tag_pattern = re.compile(r'@(\w+)(?::(.*[^\\]))?@')

    def __init__(self, s):
        self.groups = []
        def _rs(match):
            self.groups.append(match.group(1).lower())
            pattern = match.group(2) or '.+'
            return '(%s)' % pattern
        self.regex = re.compile(self._tag_pattern.sub(_rs, s))


class PatternExtractor(object):

    def __init__(self, pattern_file):
        self.patterns = []
        self.pattern_file = pattern_file
        self._read_file(pattern_file)

    def _read_file(self, filename):
        for line_num, line in enumerate(open(filename, 'r')):
            line = line.strip()
            if not line or line.startswith("#"):
                continue
            if line.startswith("include "):
                include_what = line.split()[1]
                for exp_filename in glob.glob(include_what):
                    self._read_file(exp_filename)
            else:
                try:
                    self.patterns.append(Pattern(line))
                except re.error, e:
                    log.error('Syntax error at %s line %d: %s' % (
                            filename, line_num + 1, str(e)))

    def reload(self):
        if self.pattern_file:
            self.patterns = []
            self._read_file(self.pattern_file)

    def __call__(self, meta):
        for pattern in self.patterns:
            match = pattern.regex.match(meta["msg"])
            if match:
                for i, attr in enumerate(pattern.groups):
                    meta['attr_%s' % attr] = match.group(i + 1)
        return meta
