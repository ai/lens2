// lens2.js

lens = {};

// Some constants.
lens.DEFAULT_PAGE_SIZE = 50;

// Timezone offset, in milliseconds.
lens.localTzOffset = (new Date()).getTimezoneOffset() * 60000;

lens.util = {};

/*
 * Date and timestamp helper functions.
 *
 * The following types are known:
 *
 * 'timestamp' is a standard UNIX timestamp (seconds since epoch UTC);
 * 'EStimestamp' is in microseconds.
 *
 * All dates are printed in UTC: since Javascript date support is
 * horrifying, this is achieved simply by creating bad Date objects
 * with wrong timezone information so that they display the UTC date
 * when printed...
 */
lens.util.timestampToDate = function(ts) {
    return new Date(ts * 1000 + lens.localTzOffset);
};

lens.util.dateToTimestamp = function(d) {
    return (d.getTime() / 1000);
};

lens.util.timestampNow = function() {
    return lens.util.dateToTimestamp(new Date());
};

lens.util.timestampAdd = function(ts, seconds) {
    return (ts + seconds);
};

lens.util.timestampToString = function(ts) {
    return lens.util.timestampToDate(ts).strftime('%Y/%m/%d %H:%M:%S');
};

lens.util.EStimestampToString = function(ts) {
    return lens.util.timestampToString(ts / 1000000);
};

// Template helper functions.
lens.util.replaceAttrsInMsg = function(log) {
  var msg = log.msg.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;');
  $.each(log, function(key, value) {
    if (key.substring(0, 5) == "attr_") {
      msg = msg.replace(value, '<a class="attr" name="' + key + '">'+ value + '</a>');
    }
  });
  return msg;
};

/**
 * Lens object.
 *
 * Provides all the methods to interact with the LENS2 backend.
 *
 * @param {Object} config config object
 */
lens.Lens = function(rootObj, resultViewObj, config) {
    var lensobj = this;

    // Merge config with defaults and find the elements we need.
    if (!config) {
        config = {};
    }
    var _merge = function(attr, defaultval) {
        return (config[attr] ? config[attr] : defaultval);
    };

    this.results_view_elem = resultViewObj
    //this.stats_view_elem = $(this.stats_view_id);
    this.query_field_elem = rootObj.find('.lens-query-field');
    this.search_endpoint = rootObj.find('.lens-form').attr('action');

    // Setup pagination actions.
    this.pagination_div_elem = rootObj.find('.lens-pagination-group');
    this.pagination_title_elem = this.pagination_div_elem.find('.lens-pagination-title');
    this.pagination_next_elem = this.pagination_div_elem.find('.lens-pagination-next');
    this.pagination_prev_elem = this.pagination_div_elem.find('.lens-pagination-prev');
    this.pagination_next_elem.click(function() { lensobj.pageNext(); });
    this.pagination_prev_elem.click(function() { lensobj.pagePrev(); });
    this.pagination_div_elem.hide();

    // Setup time selectors.
    this.time_start_elem = rootObj.find('.lens-time-start');
    this.time_end_elem = rootObj.find('.lens-time-end');
    var datetimepickerParams = {
        format: 'yyyy/mm/dd hh:ii',
        autoclose: true,
        todayBtn: true,
        todayHighlight: true,
        minuteStep: 10,
        pickerPosition: 'bottom-left'
    };
    this.time_start_elem.datetimepicker(datetimepickerParams).on(
        'changeDate', function(ev) {
            var t = lens.util.dateToTimestamp(ev.date);
            lensobj.query_params.time_range.start = t;
    console.log('set time_range.start orig=' + ev.date + ' dst=' + t + ' ' +
                lens.util.timestampToDate(t).strftime('%Y/%m/%d %H:%M'));
            var end_limit = t + 300;
            if (lensobj.query_params.time_range.end < end_limit) {
                lensobj.setEndTime(end_limit);
            }
        });
    this.time_end_elem.datetimepicker(datetimepickerParams).on(
        'changeDate', function(ev) {
            var t = lens.util.dateToTimestamp(ev.date);
            lensobj.query_params.time_range.end = t;
            var start_limit = t - 300;
            if (lensobj.query_params.time_range.start > start_limit) {
                lensobj.setStartTime(start_limit);
            };
        });

    // Create the rendering templates.
    var log_template_id = _merge('log_template_id', '#log_template');
    this.log_template = $.templates(log_template_id);
    var error_template_id = _merge('error_template_id', '#error_template');
    this.error_template = $.templates(error_template_id);
    var chart_template_id = _merge('chart_template_id', '#chart_template');
    this.chart_template = $.templates(chart_template_id);

    // Reset internal state.
    this.resetDefaultQueryParams();

    // Hook to hash changes for history support.
    if (window.location.hash != "") {
        this.restoreState();
    }
    $(window).on('hashchange', function() {
        lensobj.restoreState();
    });

    this.applyQueryParams();
};

lens.Lens.prototype.setStartTime = function(t) {
    this.query_params.time_range.start = t;
    console.log('set time_range.start to ' + t + ' ' +
                lens.util.timestampToDate(t).strftime('%Y/%m/%d %H:%M'));
    this.time_start_elem.val(
        lens.util.timestampToDate(t).strftime('%Y/%m/%d %H:%M'));
    this.time_start_elem.datetimepicker('update');
};

lens.Lens.prototype.setEndTime = function(t) {
    this.query_params.time_range.end = t;
    this.time_end_elem.val(
        lens.util.timestampToDate(t).strftime('%Y/%m/%d %H:%M'));
    this.time_end_elem.datetimepicker('update');
};

/* Query parameters are just a dictionary of GET params... */
lens.defaultQueryParams = function() {
    return {
        query: '',
        offset: 0,
        page_size: lens.DEFAULT_PAGE_SIZE,
        time_range: {
            start: lens.util.timestampAdd(lens.util.timestampNow(), -86400),
            end: lens.util.timestampAdd(lens.util.timestampNow(), 300),
        }
    };
};

lens.encodeQueryParamsToHash = function(p) {
    var hashTag = '#q:' + escape(p.query);
    if (p.offset > 0) {
        hashTag += ',o:' + p.offset;
    }
    if (p.page_size != lens.DEFAULT_PAGE_SIZE) {
        hashTag += ',p:' + p.page_size;
    }
    if (p.time_range.start > 0) {
        hashTag += ',trs:' + p.time_range.start;
    }
    if (p.time_range.end > 0) {
        hashTag += ',tre:' + p.time_range.end;
    }
    return hashTag;
};

lens.parseQueryParamsFromHash = function(hashstr) {
    // Ugly parsing of the hash string.
    var hashInfo = {};
    $.each(hashstr.substr(1).split(','), function(idx, value) {
        var kv = value.split(':');
        hashInfo[kv[0]] = unescape(kv.slice(1).join(':'));
    });

    // Copy the parsed data into the lens state.
    return {
        query: (hashInfo.q ? hashInfo.q : ''),
        offset: (hashInfo.o ? parseInt(hashInfo.o) : 0),
        page_size: (hashInfo.p ? parseInt(hashInfo.p) : lens.DEFAULT_PAGE_SIZE),
        time_range: {
            start: (hashInfo.trs ? parseInt(hashInfo.trs) : 0),
            end: (hashInfo.tre ? parseInt(hashInfo.tre) : 0)
        }
    };
};

/**
 * Reset query parameters to defaults.
 */
lens.Lens.prototype.resetDefaultQueryParams = function() {
    this.query_params = lens.defaultQueryParams();
};

/**
 * Make the active query parameters visible in the UI.
 */
lens.Lens.prototype.applyQueryParams = function() {
    this.query_field_elem.val(this.query_params.query);
    this.setStartTime(this.query_params.time_range.start);
    this.setEndTime(this.query_params.time_range.end);
};

/**
 * Set query (updates query field).
 */
lens.Lens.prototype.setQuery = function(query_str) {
  this.query_field_elem.val(query_str);
  this.query_params.query = query_str;
};

/**
 * Save the current state to the hashtag.
 */
lens.Lens.prototype.saveState = function() {
    var hashTag = lens.encodeQueryParamsToHash(this.query_params);
    // Order of the following statements is important: setting
    // window.location hash will trigger a call to restoreState(),
    // which will compare the hash against this.cur_hash.
    this.cur_hash = hashTag;
    window.location.hash = hashTag;
};

/**
 * Restore the current state from the location hashtag.
 */
lens.Lens.prototype.restoreState = function() {
    var hashTag = window.location.hash;
    // Return immediately if there's nothing to parse, or if we got called
    // due to having set the hashtag ourselves.
    if (!hashTag || hashTag == '#' || hashTag == this.cur_hash) {
        return;
    }

    console.log('Restoring query state from hash');

    this.query_params = lens.parseQueryParamsFromHash(hashTag);
    this.cur_hash = hashTag;
    this.doSearch();
};

/**
 * Add "onclick" handlers to the autogenerated filter links.
 */
lens.Lens.prototype.loadAttrLinks = function() {
  var lensobj = this;
  $('a.attr').click(function() {
    var attrName = $(this).attr('name');
    var attrValue = $(this).text();
    lensobj.search('(' + attrName + ':"' + attrValue + '")');
  });
};

/**
 * Add "onclick" handlers to the standard filter links (host/prog/pid).
 */
lens.Lens.prototype.loadFilterLinks = function() {
  var lensobj = this;
  $('a.host').click(function() {
    lensobj.appendToQuery('host:' + $(this).text());
  });
  $('a.facility').click(function() {
    var facility = $(this).text().split('.')[0];
    lensobj.appendToQuery('facility:' + facility);
  });
  $('a.program').click(function() {
    lensobj.appendToQuery('program:"' + $(this).text() + '"');
  });
  $('a.pid').click(function() {
    lensobj.appendToQuery('pid:' + $(this).text());
  });
};

/**
 * Render some logs into an element.
 *
 * @param {Object} dest destination element (jQuery object)
 * @param {Array[string]} logs list of logs
 */
lens.Lens.prototype.renderLogs = function(logs) {
  // TODO: Render logs in reverse order, to have the timestamps sorted
  // correctly.
  var a = [];
  $.each(logs, function(idx, entry) {
      a.push({'log': entry});
  });
  a.reverse();

  this.results_view_elem.html(
      this.log_template.render(a));

  this.results_view_elem.show();
  this.loadAttrLinks();
  this.loadFilterLinks();
};

/**
 * Update the pagination links after results have been loaded.
 */
lens.Lens.prototype.renderPagination = function(logs) {
    // Find min and max of shown timestamps.
    /**
       var ts_min = lens.util.timestampNow();
       var ts_max = 0;
       $.each(logs, function(idx, elem) {
         if (elem.timestamp > ts_max) { ts_max = elem.timestamp; }
         if (elem.timestamp < ts_min) { ts_min = elem.timestamp; }
       });
    **/

    // Update the pagination UI elements.
    if (this.query_params.offset > 0) {
        this.pagination_next_elem.show();
    } else {
        this.pagination_next_elem.hide();
    }
    if (this.query_params.offset + this.query_params.page_size < this.total_results) {
        this.pagination_prev_elem.show();
    } else {
        this.pagination_prev_elem.hide();
    }

    // Show "page numbers".
    var cur_page = 1 + Math.floor(this.query_params.offset / this.query_params.page_size);
    var tot_pages = Math.ceil(this.total_results / this.query_params.page_size);
    
    this.pagination_title_elem.text('' + cur_page + '/' + tot_pages);

    this.pagination_div_elem.show();
};

lens.Lens.prototype.renderChart = function(chart_attr, chart_title, chart_data) {
  $.tmpl(this.chart_template,
         {name: chart_attr, title: chart_title}
        ).appendTo(this.stats_view_id);
  Donut(this, chart_attr, 'chart_graph_' + chart_attr).data(chart_data).draw();
};

lens.Lens.prototype.renderHistogram = function(chart_attr, chart_title, chart_data) {
  $.tmpl(this.chart_template,
         {name: chart_attr, title: chart_title}
        ).appendTo(this.stats_view_id);
  Timeline('chart_graph_' + chart_attr).data(chart_data).draw();
};

lens.Lens.prototype.render = function(logs, facets, elapsed) {
  // Render the logs.
  if (logs.length > 0) {
    this.renderLogs(logs);
  } else {
    this.results_view_elem.html('<b>No results found</b>').show();
  }

  // Render pagination elements.
  this.renderPagination(logs);

  // Render stats.
  /**
  this.stats_view_elem.html('');
  if (facets.host) {
    this.renderChart('host', 'Top 5 hosts', facets.host.terms);
  }
  if (facets.program) {
    this.renderChart('program', 'Top 5 programs', facets.program.terms);
  }
  //if (facets.timestamp) {
  //  this.renderHistogram('timestamp', 'Time distribution',
  //                       facets.timestamp.entries);
  //}
  this.stats_view_elem.show();
  **/

  // Show query time.
  //$('#query_time').text('query time: ' + elapsed + ' ms');
};

/**
 * Refresh the view with the current query and params.
 */
lens.Lens.prototype.doSearch = function() {
    // Ensure that the UI matches the search parameters.
    this.applyQueryParams();
    this.clearError();
    this.showLoading();
    this.pagination_div_elem.hide();

    /**
    // Autodetect whether to extend the time range.
    if ((this.total_results > 0) &&
        (this.query_params.offset + this.query_params.page_size > this.total_results)) {
        this.query_params.time_range.start = lens.util.timestampAdd(this.query_params.time_range.start, -86400);
    }
    **/

    // Perform the query.
    console.log('Query: ' + JSON.stringify(this.query_params));

    var lensobj = this;
    var time_range_str = (
        '' + Math.floor(this.query_params.time_range.start) +
            ':' + (
                (this.query_params.time_range.end > 0)
                    ? Math.floor(this.query_params.time_range.end)
                    : lens.util.timestampNow()));

    $.ajax({
        url: this.search_endpoint,
        type: 'POST',
        data: {q: this.query_params.query,
               start: this.query_params.offset,
               page_size: this.query_params.page_size,
               time_range: time_range_str,
               facets: 'timestamp'
              },
        dataType: 'json',
        success: function(data, status, jqxhr) {
            if (data.error) {
                lensobj.ajaxError(jqxhr, 'query_error', data.error);
            } else {
                lensobj.total_results = data.total_results;
                lensobj.render(data.results, data.facets, data.elapsed);
                lensobj.clearLoading();
                lensobj.saveState();
            }
        },
        error: this.ajaxError,
        context: lensobj
    });
};

/**
 * Search.
 */
lens.Lens.prototype.search = function(query_str) {
    this.setQuery(query_str);
    this.query_params.offset = 0;
    this.applyQueryParams();
    this.doSearch();
};

// Functions to manipulate the current query.
lens.Lens.prototype.appendToQuery = function(add_str) {
  if (!this.query_params.query.match(add_str)) {
    this.search(this.query_params.query + ' AND ' + add_str);
  }
};

/**
 * Move to the previous page.
 */
lens.Lens.prototype.pagePrev = function() {
  if (this.query_params.offset < this.total_results) {
    this.query_params.offset += this.query_params.page_size;
    if (this.query_params.offset >= this.total_results) {
      this.query_params.offset = this.total_results - 1;
    }
    this.doSearch();
  }
};

/**
 * Move to the next page.
 */
lens.Lens.prototype.pageNext = function() {
  if (this.query_params.offset > 0) {
    this.query_params.offset -= this.query_params.page_size;
    if (this.query_params.offset < 0) {
      this.query_params.offset = 0;
    }
    this.doSearch();
  }
};

/**
 * General AJAX error callback.
 */
lens.Lens.prototype.ajaxError = function(jqxhr, textStatus, errorThrown) {
  this.clearLoading();
  $('#error').attr(
      'data-title',
      this.error_template.render(
          {error: textStatus,
           errorThrown: errorThrown}));
  this.pagination_div_elem.hide();
  this.results_view_elem.html('');
  $('#error').show();
  $('#error').tooltip('show');
};

/**
 * Clear error message.
 */
lens.Lens.prototype.clearError = function() {
  $('#error').hide();
};

/**
 * Show 'loading' icon.
 */
lens.Lens.prototype.showLoading = function() {
    // $('#loading').show();
    this.query_field_elem.toggleClass('loading', true);
};

/**
 * Hide 'loading' icon.
 */
lens.Lens.prototype.clearLoading = function() {
    // $('#loading').hide();
    this.query_field_elem.toggleClass('loading', false);
};

