import re
import time
from datetime import datetime, tzinfo, timedelta


class UTC(tzinfo):
    def tzname(self, dt):
        return 'UTC'
    def utcoffset(self, dt):
        return timedelta(0)
    def dst(self, dt):
        return timedelta(0)

_utc = UTC()

# Make this a local function to simplify testing.
def _now():
    return datetime.utcnow()


def parse_iso8601_date(s):
    return datetime.strptime(s[:19], "%Y-%m-%dT%H:%M:%S").replace(tzinfo=_utc)


def parse_plain_date(s):
    return datetime.strptime(s, "%Y%m%d%H%M%S").replace(tzinfo=_utc)


def parse_timespec(s):
    sfx_to_sec = {'s': 1, 'm': 60, 'h': 3600, 'd': 86400, 'w': 604800,
                  'M': 2592000, 'y': 31536000}
    m = re.search(r'^(\d+)\s*([smhdwMy])?$', s)
    if m:
        spec = int(m.group(1))
        if m.group(2):
            spec *= sfx_to_sec[m.group(2)]
            return _now() - timedelta(0, spec)
        else:
            return datetime.utcfromtimestamp(spec)


def parse_datespec(s):
    m = re.search(r'^(\d{4})[-/](\d{1,2})[-/](\d{1,2})$', s)
    if m:
        return datetime.strptime('/'.join(m.groups()), '%Y/%m/%d')
    m = re.search(r'^(\d{1,2})[-/](\d{1,2})[-/](\d{4})$', s)
    if m:
        return datetime.strptime('/'.join(m.groups()), '%d/%m/%Y')


def parse_time_range(s):
    t = parse_timespec(s)
    if t:
        return (t, None)
    t = parse_datespec(s)
    if t:
        return (t, t + timedelta(1))
    if ':' in s:
        def _parse_elem(x):
            return parse_timespec(x) or parse_datespec(x)
        start, end = map(_parse_elem, s.split(':', 1))
        return (start, end)


def batch(iterator, n=100):
    """Group elements of 'iterator' in batches of size 'n'."""
    buf = []
    for item in iterator:
        buf.append(item)
        if len(buf) >= n:
            yield buf
            buf = []
    if buf:
        yield buf


def format_log(log):
    """Print 'log' in syslog-compatible format."""
    tag = log['program']
    if 'pid' in log:
        tag = '%s[%s]' % (tag, log['pid'])
    return '%s %s %s.%s %s: %s\n' % (
        log['timestamp'].strftime('%Y-%m-%dT%H:%M:%S'), log['host'],
        log.get('facility', '<none>'), log.get('severity', '<none>'),
        tag, log['msg'])
