import unittest
from datetime import datetime, timedelta
from lens2 import utils


class UtilsTest(unittest.TestCase):

    def setUp(self):
        # Stub out utils._now
        self.now = datetime.utcnow()
        def _now():
            return self.now
        self.old_now = utils._now
        utils._now = _now

    def tearDown(self):
        utils._now = self.old_now

    def test_parse_iso8601_date(self):
        d = datetime(2010, 3, 13, 7, 28, 45, 0, utils.UTC())
        self.assertEquals(d, utils.parse_iso8601_date('2010-03-13T07:28:45'))

    def test_parse_timespec(self):
        data = [
            ('1d', (self.now - timedelta(1, 0), None)),
            ('2d', (self.now - timedelta(2, 0), None)),
            ('2d:1d', (self.now - timedelta(2, 0), self.now - timedelta(1, 0))),
            ('2010/12/13', (datetime(2010, 12, 13, 0, 0, 0),
                            datetime(2010, 12, 14, 0, 0, 0))),
            ('2010/12/13:2011/01/01', (datetime(2010, 12, 13, 0, 0, 0),
                                       datetime(2011, 01, 01, 0, 0, 0))),
            ]
        for spec, expected_range in data:
            result = utils.parse_time_range(spec)
            self.assertEquals(expected_range, result,
                              "parse_time_range('%s') failed: %s (expected: %s)" % (
                    spec, result, expected_range))

    def test_format_log(self):
        log = {'timestamp': datetime(2010, 12, 13, 1, 2, 3, 0, utils.UTC()),
               'host': 'server', 'program': 'sshd', 'pid': 1234,
               'facility': 'auth', 'severity': 'info',
               'msg': 'Something'}
        expected = '2010-12-13T01:02:03 server auth.info sshd[1234]: Something\n'
        self.assertEquals(expected,
                          utils.format_log(log))
        del log['pid']
        expected = '2010-12-13T01:02:03 server auth.info sshd: Something\n'
        self.assertEquals(expected,
                          utils.format_log(log))

    def test_parse_time_range(self):
        self.assertEquals(
            (self.now - timedelta(3), None),
            utils.parse_time_range('3d'))
        
        
