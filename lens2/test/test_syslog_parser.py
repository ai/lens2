import time
import unittest
from datetime import datetime
from lens2 import syslog_parser
from lens2 import utils


LOGS = [
    "Mar 13 07:28:45 sleppa daemon.info ntpd[5867]: ntpd 4.2.4p4@1.1520-o Fri Dec  4 18:18:33 UTC 2009 (1)",
    "Mar 13 07:28:45 sleppa daemon.info ntpd[5867]: kernel time sync status 0040",
    "Mar 13 07:28:50 sleppa kernel.info kernel: [   17.939952] wlan0: no IPv6 routers present",
    ]


class SyslogParserTest(unittest.TestCase):

    def test_syslog_parser(self):
        parser = syslog_parser.SyslogParser()
        # parse the logs, discard key information
        out = [parser(x) for x in LOGS]
        # check host
        for entry in out:
            self.assertEquals("sleppa", entry["host"])
        # check timestamps
        cur_year = datetime.now().year
        t0 = time.mktime(datetime(cur_year, 3, 13, 7, 28, 45, 0, utils.UTC()).utctimetuple())
        self.assertEquals(t0, out[0]["timestamp"])
        t2 = time.mktime(datetime(cur_year, 3, 13, 7, 28, 50, 0, utils.UTC()).utctimetuple())
        self.assertEquals(t2, out[2]["timestamp"])
        # check facility
        self.assertEquals("daemon", out[0]["facility"])
        self.assertEquals("kernel", out[2]["facility"])
        # check severity
        self.assertEquals("info", out[0]["severity"])
        self.assertEquals("info", out[2]["severity"])
        # check prog
        self.assertEquals("ntpd", out[0]["program"])
        self.assertEquals("ntpd", out[1]["program"])
        self.assertEquals("kernel", out[2]["program"])
        # check pid
        self.assertEquals("5867", out[0]["pid"])
        self.assertEquals("5867", out[1]["pid"])
        self.assertFalse("pid" in out[2])
        # check messages
        self.assert_("ntpd" in out[0]["msg"])
        self.assert_("sync status" in out[1]["msg"])
        self.assert_("wlan0" in out[2]["msg"])


if __name__ == "__main__":
    unittest.main()

