

def read_file(path):
    with open(path, 'rb') as fd:
        for line in fd:
            try:
                line = unicode(line, 'utf-8', 'replace')
            except UnicodeDecodeError:
                continue
            yield line.rstrip('\n')


def read_fifo(fifo_path):
    while True:
        for line in read_file(fifo_path):
            yield line

