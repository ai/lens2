import os
import re
import time
from lens2 import utils


_tag_pattern = re.compile(r'^(.+)\[(\d+)\]:?$')

def _parse_tag(tag):
    match = _tag_pattern.search(tag)
    if match:
        return (match.group(1), match.group(2))
    else:
        return (tag.rstrip(':'), None)

_parser_registry = {}

def register_parser(name, class_):
    _parser_registry[name] = class_

def get_parser(name, **args):
    return _parser_registry[name](**args)

def list_parsers():
    return _parser_registry.keys()


class SyslogParserBase(object):
    """Base parser class for syslog data."""

    def parse_date(self, line):
        raise NotImplementedError()

    def __call__(self, line):
        stamp, _msg = self.parse_date(line)
        host, pri, tag, msg = _msg.split(' ', 3)
        prog, pid = _parse_tag(tag)
        facility, severity = pri.split('.', 1)
        info = {'timestamp': stamp, 'host': host,
                'facility': facility, 'severity': severity,
                'program': prog, 'msg': msg}
        if pid:
            info['pid'] = pid
        return info


class SyslogIsoParser(SyslogParserBase):
    """Syslog parser with ISO-8601 dates.

    The format supported by this class is not the default syslogd one
    (which lacks full timestamp and priority information). Use this
    rsyslogd log template instead:

    $template localFormat,"%timereported:::date-rfc3339% %HOSTNAME% %syslogfacility-text%.%syslogseverity-text% %syslogtag%%msg::space%\n"

    Alternatively, for syslog-ng:

    template("${ISODATE} ${HOST} ${FACILITY}.${PRIORITY} ${MSGHDR}${MSG}\n")

    """

    def parse_date(self, line):
        _stamp, _msg = line.split(' ', 1)
        stamp = utils.parse_iso8601_date(_stamp)
        return stamp, _msg

register_parser('iso', SyslogIsoParser)


class SyslogParser(SyslogParserBase):
    """Standard syslog format parser."""

    def __init__(self, tz=None):
        self.tz = tz            # ignored for now
        self.year = time.gmtime().tm_year

    def parse_date(self, line):
        _stamp, msg = line[:15], line[16:]
        stamp_tuple = time.strptime(_stamp, '%b %d %H:%M:%S')
        fixed_tuple = (self.year, stamp_tuple.tm_mon, stamp_tuple.tm_mday,
                       stamp_tuple.tm_hour, stamp_tuple.tm_min,
                       stamp_tuple.tm_sec, stamp_tuple.tm_wday,
                       stamp_tuple.tm_yday, -1)
        return time.mktime(fixed_tuple), msg

register_parser('standard', SyslogParser)


class SyslogDumbParser(SyslogParser):

    def __call__(self, line):
        stamp, rest = self.parse_date(line)
        host, tag, msg = rest.split(' ', 2)
        prog, pid = _parse_tag(tag)
        info = {'timestamp': stamp, 'host': host,
                'program': prog, 'msg': msg}
        if pid:
            info['pid'] = pid
        return info

register_parser('dumb', SyslogDumbParser)
