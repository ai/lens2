from flask import Blueprint, Flask, abort, redirect, request, \
    g, current_app, render_template

ui_app = Blueprint('ui', __name__,
                   template_folder='templates')


@ui_app.route('/')
def ui_index():
    return render_template('index.html')
