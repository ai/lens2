import logging
from lens2 import utils
from flask import Blueprint, Flask, abort, redirect, request, \
    g, current_app, jsonify

api_app = Blueprint('api', __name__)
log = logging.getLogger(__name__)


@api_app.route('/search', methods=['POST'])
def api_search():
    facets = [('host', 5), ('program', 5)]
    try:
        query = request.form['q']
        start = int(request.form.get('start', 0))
        page_size = int(request.form.get('page_size', 20))
        time_range = utils.parse_time_range(
            request.form.get('time_range', '1d'))
        for facet_spec in request.form.get('facets', '').split():
            if ':' in facet_spec:
                facet_term, facet_size = facet_spec.split(':')
            else:
                facet_term, facet_size = facet_spec, 5
            facets.append((facet_term, int(facet_size)))
    except (KeyError, ValueError):
        log.exception('search not parsed')
        abort(400)

    try:
        results, total, facet_data, elapsed = current_app.logstore.search(
            query, time_range, start=start, size=page_size, facets=facets)
        return jsonify(results=results,
                       total_results=total,
                       facets=facet_data,
                       elapsed=elapsed)
    except Exception, e:
        return jsonify(error=str(e))


@api_app.route('/status')
def api_status():
    index_stats = current_app.logstore.get_status()
    return jsonify(docs=index_stats['docs'],
                   index=index_stats['index'],
                   merges=index_stats['merges'])

