#!/usr/bin/python

import os
import sys
from datetime import datetime


def lens2syslog(fd):
    """Convert ISO timestamps in the lens2 output to syslog-like format.

    Ignores timezone information (it simply keeps the original timezone).
    """
    for line in fd:
        try:
            timespec, host, level, rest = line.split(' ', 3)
            iso_spec, tz_h, tz_m = timespec[:19], int(timespec[20:22]), int(timespec[23:25])
            stamp = datetime.strptime(iso_spec, '%Y-%m-%dT%H:%M:%S')
            fmtstamp = stamp.strftime('%b %e %H:%M:%S')
            sys.stdout.write('%s %s %s' % (fmtstamp, host, rest))
        except:
            sys.stdout.write(line)


def main():
    if len(sys.argv) > 2:
        print >>sys.stderr, 'Usage: lens2syslog [<INPUT_FILE>]'
        sys.exit(1)
    if len(sys.argv) == 2:
        fd = open(sys.argv[1], 'r')
    else:
        fd = sys.stdin
    lens2syslog(fd)


if __name__ == '__main__':
    main()


