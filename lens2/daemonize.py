import logging
import logging.handlers
import os
import pwd
import sys
import signal

log = logging.getLogger(__name__)
program_name = os.path.basename(sys.argv[0])


class PIDFile(object):

    def __init__(self, path):
        self.path = path

    def acquire(self):
        if os.path.exists(self.path):
            cur_pid = -1
            try:
                cur_pid = int(open(self.path).read().strip())
            except:
                sys.stderr.write(
                    'Warning: corrupted pidfile, overwriting...\n')
            if cur_pid > 0:
                try:
                    os.kill(cur_pid, 0)
                    raise RuntimeError(
                        'another instance is already running, pid=%d' % cur_pid)
                except OSError:
                    sys.stderr.write(
                        'Warning: stale lock found (pid=%d)\n' % cur_pid)
        fd = open(self.path, 'w')
        fd.write('%d\n' % os.getpid())
        fd.close()

    def release(self):
        try:
            os.unlink(self.path)
        except:
            pass


def setup_logging(foreground=False, debug=False):
    logger = logging.getLogger()
    if foreground:
        logging.basicConfig()
    else:
        handler = logging.handlers.SysLogHandler(address='/dev/log',
                                                 facility=3)
        handler.setFormatter(logging.Formatter(
                program_name + '[%(process)d]: %(levelname)s: %(message)s'))
        logger.addHandler(handler)
    if debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)


def daemonize(opts, main_function, *args):
    """Our own daemonizing function.

    It will daemonize (or not) 'main_function' according to the
    command-line parameters specified in 'opts'.

    Supported options:

      - opts.pidfile: where to write the PID file
      - opts.debug: set logging level to DEBUG
      - opts.foreground: do not detach from running terminal
      - opts.username: setuid() to this user

    We don't want to close the existing file descriptors, as gevent
    and logging need some of them.
    """
    if opts.username:
        try:
            os.setuid(pwd.getpwnam(opts.username).pw_uid)
        except KeyError:
            sys.stderr.write(
                'Error: could not find user "%s"\n' % opts.username)
            sys.exit(1)

    if not opts.foreground:
        if os.fork():
            return

    setup_logging(opts.foreground, opts.debug)

    if not opts.foreground:
        pidfile = PIDFile(opts.pidfile)
        try:
            pidfile.acquire()
        except Exception, e:
            log.fatal('could not write pidfile: %s' % str(e))
            sys.exit(1)

    def _sighandler(signum, frame):
        log.info('terminated by signal %d' % signum)
        try:
            if not opts.foreground:
                pidfile.release()
        except Exception, e:
            log.error('could not remove pidfile: %s' % e)
        sys.exit(0)
    signal.signal(signal.SIGTERM, _sighandler)
    signal.signal(signal.SIGINT, _sighandler)

    if not opts.foreground:
        null_fd = open(os.devnull, 'r+')
        for stream in (sys.stdin, sys.stdout, sys.stderr):
            os.dup2(null_fd.fileno(), stream.fileno())
        os.setsid()

    try:
        main_function(*args)
    except Exception:
        log.exception('Unexpected exception')
    finally:
        if not opts.foreground:
            pidfile.release()


def add_options(parser):
    """Add some standard options to an OptionParser instance.

    The options are those mentioned in the 'daemonize' docstring.
    """
    parser.add_option('--debug', dest='debug', action='store_true',
                      help='Set log level to DEBUG')
    parser.add_option('--foreground', dest='foreground', action='store_true',
                      help='Do not fork into the background')
    parser.add_option('--pidfile', dest='pidfile', metavar='FILE',
                      default='/var/run/%s.pid' % program_name,
                      help='Location of the pid file')
    parser.add_option('--user', dest='username', metavar='NAME',
                      help='Optionally run as this user')
